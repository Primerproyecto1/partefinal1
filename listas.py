#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import time
from subprocess import call
lista = []
lista_final = []
inicio = 0
vecino_vivo = 0

tamano = int(input("Ingrese el tamaño de la lista: "))		
# ~ condiciones
# ~ print(tamano)
def condiciones(i, inicio, tamano, lista, vecino_vivo):
	while (i < (tamano * tamano)):
		# ~ condiciones para el lado izquierdo
		if (i == 0) or (i == inicio):
			inicio = inicio + tamano
			if (lista[i] == "[X]"):
				# ~ print("VIVO")
				if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBA")
				if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBADERECHA")
				if(lista[i + 1] == "[X]"):
					vecino_vivo = vecino_vivo + 1
					# ~ print("DERECHA")
				if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJOM")
				if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJODERECHA")
			if(lista[i] == "[-]"):
				# ~ print("MUERTOS")
				if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBA")
				if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBADERECHA")
				if(lista[i + 1] == "[X]"):
					vecino_vivo = vecino_vivo + 1
					# ~ print("DERECHA")
				if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJO")
				if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJODERECHA")
		# ~ condiciones para el lado derecho
		elif (((i + 1) % tamano) == 0):
			if (lista[i] == "[X]"):
				# ~ print("VIVOS")
				if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBA")
				if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJO")
				if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBAIZQ")
				if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("IZQ")
				if(((i + tamano - 1)  < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJOIZQ")
			if(lista[i] == "[-]"):
				# ~ print("MUERTOS")
				if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBA")
				if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJO")
				if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBAIZQ")
				if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("IZQ")
				if(((i + tamano - 1)  < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJOIZQ")
		# ~ condiciones para las que estan al medio
		else:
			if (lista[i] == "[X]"):
				# ~ print("VIVA")
				if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBA")
				if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBADERECHA")
				if((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]"):
					vecino_vivo = vecino_vivo + 1
					# ~ print("DERECHA")
				if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJO")
				if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJODERECHA")
				if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJOIZQ")
				if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("IZQ")
				if(((i + tamano - 1)  < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJOIZQ")
			if (lista[i] == "[-]"):
				# ~ print("MUERTA")
				if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBA")
				if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBADERECHA")
				if((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]"):
					vecino_vivo = vecino_vivo + 1
					# ~ print("DERECHA")
				if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJO")
				if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJODERECHA")
				if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ARRIBAIZQ")
				if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("IZQ")
				if(((i + tamano - 1)  < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
					vecino_vivo = vecino_vivo + 1
					# ~ print("ABAJOIZQ")
		# ~ print(vecino_vivo)
		reglas(i, vecino_vivo, lista, lista_final)
		i= i + 1 
		vecino_vivo = 0
	return lista_final
		
def reglas(i, vecino_vivo, lista, lista_final):	
#condiciones de las celulas 
	if((lista [i] == "[X]") and ((vecino_vivo == 2) or (vecino_vivo == 3))):
		lista_final.append("[X]")
		# ~ print ("La celula sobrevive")
	elif((lista [i] == "[X]") and (vecino_vivo >= 4)):
		lista_final.append("[-]")
		# ~ print ("La celula muere por sobrepoblacion")
	elif((lista [i] == "[X]") and (vecino_vivo <= 1)):
		lista_final.append("[-]")
		# ~ print ("La celula muere por soledad")
	elif((lista[i] == "[-]") and (vecino_vivo == 3)):
		lista_final.append("[X]")
		# ~ print ("La celula volvera a vivir")
	else:
		lista_final.append("[-]")
	return lista_final
def continuidad(tamano, validacion_juego, lista_final):
	stop = 1
	validacion_juego = 0
	for i in range(tamano * tamano):
		if(lista_final[i] == "[-]"):
			validacion_juego = validacion_juego + 1
	if(validacion_juego == (tamano * tamano)):
		stop = 0
	# ~ print (stop)
	# ~ print(validacion_juego)
	# ~ print (tamano * tamano)
	return stop
def imprimir(tamano, lista_final):
	for j in range(tamano * tamano):
		print (lista_final[j], end = "")
		if ((j + 1) % tamano == 0):
			print("\n")

for i in range(tamano * tamano):
	estado = random.randint(0, 1)
	if (estado == 0):
		lista.append("[X]")
	else:
		lista.append("[-]")
	print(lista[i], end = "")
	if ((i + 1) % tamano == 0):
		print("\n")
stop = 1
while (stop != 0):
	validacion_juego = 1
	i = 0
	condiciones(i, inicio, tamano, lista, vecino_vivo)
	time.sleep(2.4)
	call('clear')
	imprimir(tamano, lista_final)
	stop = continuidad(tamano, validacion_juego, lista_final)
	# ~ print (stop)
	# ~ print(validacion_juego)
	# ~ print (tamano*tamano)
	i = 0 
	for i in range(tamano * tamano):
		lista[i] = lista_final[i]
	lista_final = []
